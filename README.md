# TOK - Translational Oncology Knowledgebase 

## General info

TOK is a gene-centric database aggregator, which merges mutation, drug sensitivity, and phenotype
related data from external sources. Each source has a metadata configuration, which specifies how
the original raw data should be mapped and summarized.
The application also harmonizes gene symbols and drug names in order to provide consistent search
functionality and results.

## Requirements

Python 3, SQLite, Flask, pyaml, requests

These are conveniently included in Anaconda: https://www.anaconda.com/download/

Alternatively the packages can be managed with pip and virtualenv.

## QuickStart

1. Download the datasources and metadatas. TODO: From where?
2. Build the database

    `database/build.py /path/to/data/sources`

3. Launch the webapp

    `cd web`  
    `./start.sh`

4. Browse to http://localhost:5000/

## Folders

* *database/* — scripts and some supporting data for building the SQLite database.
* *web/* — a web app that allows for browsing the contents of the
database.
* *utils/* — miscellaneous utility scripts

The folders may contain further documentation in README.md files.

## License

Two-clause BSD license. See `LICENSE` file.

## Authors

* Kari Lavikka - kari.lavikka@helsinki.fi
* Jaana Oikkonen - jaana.oikkonen@helsinki.fi

## Contributors

* Rainer Lehtonen
* Anniina Färkkilä
* Ville Rantanen
* Erika Ojanperä
