# Annitator Web

## Usage

1. Build the database. See ../database/README.md
2. Install Flask: http://flask.pocoo.org/
3. Start using ./start.sh ... or with a method of your choice
4. Follow the instructions in the console for accessing on localhost

## Usage with Nginx

Nginx communicates with Python apps through FastCGI.
See http://flask.pocoo.org/docs/0.12/deploying/fastcgi/

*annitator.fcgi* script launches the web application and creates a FastCGI
socket at /tmp/annitator-web.fcgi
