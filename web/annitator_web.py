"""
A Web interface to Annitator
"""

# Author: Kari Lavikka <kari.lavikka@helsinki.fi>

import sqlite3
import html
import itertools
import io
import csv
import re
from collections import defaultdict
from flask import Flask, g, render_template, request, jsonify, abort, Response

# Please, do not put the database in VCS!
db_file = "../database/annitator.db"

app = Flask(__name__)

@app.route('/')
def main():
    terms = dict()
    errors = list()

    def add_if_present(name):
        value = request.args.get(name)
        if value is not None and value != "":
            terms[name] = value.strip()

    for term in ["gene", "drug", "disease", "summary", "evidence", "variant"]:
        add_if_present(term)

    try:
        preprocess_terms(terms)

        if terms:
            fragments = build_where_fragments(terms)

            results = {
                "aberrations": query_gene_aberration(fragments),
                "aberration_drugs": query_gene_aberration_drugs(fragments),
                "aberration_diseases": query_gene_aberration_diseases(fragments)
            }

        else:
            results = dict()
            
    except ValueError as error:
        results = dict()
        errors.append(str(error))
        print("KUKKUUU!")


    format = request.args.get("format", "html")

    if format == "html":
        template_args = {
            "query_string": request.query_string.decode("ASCII"),
            "form": request.args.to_dict(),
            "results": results,
            "errors": errors
        }
        
        return result_to_html(template_args)

    elif format == "tsv":
        if len(errors) == 0:
            return result_to_tsv(results)
        else:
            return errors[0], 400

    elif format == "count":
        if len(errors) == 0:
            # TODO: Optimize query
            return str(len(results["aberrations"]))
        else:
            return errors[0], 400

    else:
        return None


@app.route('/search/disease/<disease>')
def search_disease(disease):
    return jsonify([dict(row) for row in query_disease('%' + disease + '%')])


@app.route('/search/drug/<drug>')
def search_drug(drug):
    return jsonify([dict(row) for row in query_drug('%' + drug + '%')])


@app.route('/search/gene/<gene>')
def search_gene(gene):
    return jsonify([dict(row) for row in query_gene_summary(gene + '%')])


@app.route('/details/<int:gene_aberration_id>')
def show_details(gene_aberration_id):
    details = query_gene_aberration_details(gene_aberration_id)
    
    where_fragments = (
        ["gene_aberration_id = ?"],
        [gene_aberration_id])

    details["drugs"] = query_gene_aberration_drugs(where_fragments)[gene_aberration_id]
    details["diseases"] = query_gene_aberration_diseases(where_fragments)[gene_aberration_id]
    details["raw_data"] = query_raw_data(details["table"]["name"], details["gene_aberration"]["raw_id"])

    if details:
        return render_template("gene_aberration_details.html", details=details)
    else:
        abort(404)


@app.route('/database_summary')
def show_database_summary():
    summary = query_database_summary()

    return render_template("database_summary.html", articles=summary)


def preprocess_terms(terms):

    def get_unambigious_id(results):
        if len(results) == 1:
            return results[0]["id"]
        else:
            return None


    if "drug" in terms:
        drug_id = get_unambigious_id(query_drug(terms["drug"]))
        if drug_id is not None:
            terms["drug_id"] = drug_id
        del terms["drug"]

    if "disease" in terms:
        disease_id = get_unambigious_id(query_disease(terms["disease"]))
        if disease_id is not None:
            terms["disease_id"] = disease_id
        del terms["disease"]
    
    if "variant" in terms:
        match = re.search('([a-zA-Z0-9]+):([pc])\.(.*)', terms["variant"])
        if match:
            if "gene" in terms:
                raise ValueError("Define either gene or variant, not both!")

            terms["gene"] = match.group(1)
            variant_type = match.group(2)
            variant = match.group(3)

            if variant_type == "p":
                terms["variant_protein"] = variant
            elif variant_type == "c":
                terms["variant_DNA"] = variant

        else:
            raise ValueError("The variant is formatted incorrectly")


def build_where_fragments(terms):
    fragments = []
    values = []
   
    if "gene" in terms:
        fragments.append("ga.gene LIKE ?")
        values.append(terms["gene"])
 
    if "summary" in terms:
        fragments.append("ga.summary LIKE ?")
        values.append("%" + terms["summary"] + "%")
 
    if "drug_id" in terms:
        # EXISTS appears to be slow in SQLite. Let's use an equivalent IN subquery
        #fragments.append("EXISTS (SELECT 'x' FROM gene_aberration_drug gad WHERE gad.gene_aberration_id = ga.gene_aberration_id AND gad.drug_id = ?)")
        fragments.append("gene_aberration_id IN (SELECT gene_aberration_id FROM gene_aberration_drug gad WHERE gad.drug_id = ?)")
        values.append(terms["drug_id"])

    if "disease_id" in terms:
        #fragments.append("EXISTS (SELECT 'x' FROM gene_aberration_disease gad WHERE gad.gene_aberration_id = ga.gene_aberration_id AND gad.disease_id = ?)")
        fragments.append("gene_aberration_id IN (SELECT gene_aberration_id FROM gene_aberration_disease gad WHERE gad.disease_id = ?)")
        values.append(terms["disease_id"])

    if "evidence" in terms:
        fragments.append("ga.evidence_level >= ?")
        values.append(int(terms["evidence"]))

    if "variant_protein" in terms:
        fragments.append("ga.variant_protein LIKE ?")
        values.append(terms["variant_protein"] + "%")

    if "variant_DNA" in terms:
        fragments.append("ga.variant_DNA LIKE ?")
        values.append(terms["variant_DNA"] + "%")
 
    return (fragments, values)
    

def query_gene_aberration_details(gene_aberration_id):
    
    included_columns = {
        "article": [
            "title", "short_title", "very_short_title", "source_type",
            "journal", "year", "abstract", "doi", "PMID", "url",
            "date_of_db_update", "genome_version"],
        "table": [
            "name", "description", "additional_info", "main_category"],
        "gene_aberration": [
            "raw_id", "gene", "summary",
            "clinical_significance", "variant_protein", "variant_id",
            "haplotype_id", "variant_DNA", "cnv", "expression", "methylation",
            "evidence_level"] }

    # prefix all column names with the table names
    select = ",\n".join(itertools.chain.from_iterable([
        ['"{}"."{}" AS "{}_{}"'.format(table, column, table, column) for column in columns]
        for table, columns in included_columns.items()]))

    sql = "SELECT " + select + """
        FROM gene_aberration 
        INNER JOIN "table" USING (table_id) 
        INNER JOIN article USING (article_id) 
        WHERE gene_aberration_id = ?"""

    conn = get_db()
    cursor = conn.cursor()
    
    results = cursor.execute(sql, (gene_aberration_id, ))
    try:
        for result in results:
            return {table: {col: result[table + '_' + col] for col in cols} for (table, cols) in included_columns.items()}

        return None

    finally:
        cursor.close()


def query_raw_data(table, raw_id):
    sql = 'SELECT * FROM "{}" WHERE raw_id = ?'.format(table)

    conn = get_db()
    cursor = conn.cursor()
    
    results = cursor.execute(sql, (raw_id, ))
    try:
        for result in results:
            return dict(result)

        return None

    finally:
        cursor.close()


def query_gene_aberration(where_fragments):
    """Executes a query using the given search terms and returns the results as a list"""

    # TODO: Receive the search terms as a dictionary and build the sql dynamically

    conn = get_db()
    cursor = conn.cursor()

    sql = """SELECT
            ga.gene_aberration_id,
            coalesce(a.very_short_title, a.short_title, a.title) AS title,
            coalesce(a.short_title, a.title) AS longer_title,
            a.journal,
            t.name AS table_name,
            t.description AS table_description,
            ga.*
        FROM gene_aberration ga
        INNER JOIN "table" t USING (table_id)
        INNER JOIN article a USING (article_id)
        WHERE """ + " AND ".join(where_fragments[0]) + """
        ORDER BY a.journal, t.name, ga.evidence_level DESC"""

    results = cursor.execute(sql, where_fragments[1]).fetchall()
    cursor.close()

    return results


def query_gene_aberration_drugs(where_fragments):
    """Executes a query and returns a dictionary that contains all drugs that
    are related to a specific gene aberrations."""

    conn = get_db()
    cursor = conn.cursor()
    
    sql = """SELECT
            ga.gene_aberration_id, d.drug_id, d.pubchem_compound_id, d.name
        FROM gene_aberration_drug
        INNER JOIN gene_aberration ga USING (gene_aberration_id)
        INNER JOIN "table" t USING (table_id)
        INNER JOIN article a USING (article_id)
        INNER JOIN drug d USING (drug_id)
        WHERE """ + " AND ".join(where_fragments[0])

    gene_aberration_drugs = defaultdict(lambda: [])

    for row in cursor.execute(sql, where_fragments[1]):
        gene_aberration_drugs[row[0]].append({
            "id": row[1],
            "cid": row[2],
            "name": row[3]
        })

    cursor.close()
        
    return gene_aberration_drugs


def query_gene_aberration_diseases(where_fragments):
    """Executes a query and returns a dictionary that contains all diseases that
    are related to a specific gene aberrations."""

    conn = get_db()
    cursor = conn.cursor()
    
    sql = """SELECT
            ga.gene_aberration_id, d.disease_id, d.TCGA_disease, d.full_name
        FROM gene_aberration_disease
        INNER JOIN gene_aberration ga USING (gene_aberration_id)
        INNER JOIN "table" t USING (table_id)
        INNER JOIN article a USING (article_id)
        INNER JOIN disease d USING (disease_id)
        WHERE """ + " AND ".join(where_fragments[0])

    gene_aberration_diseases = defaultdict(lambda: [])

    for row in cursor.execute(sql, where_fragments[1]):
        gene_aberration_diseases[row[0]].append({
            "id": row[1],
            "TCGA_disease": row[2],
            "name": row[3]
        })

    cursor.close()
        
    return gene_aberration_diseases


def query_drug(name):
    conn = get_db()
    cursor = conn.cursor()
  
    try:
        return cursor.execute(
            "SELECT drug_id AS id, pubchem_compound_id, name, name AS preferred_name, gene_aberration_occurences FROM drug WHERE name LIKE ? "
            "UNION "
            "SELECT drug_id AS id, pubchem_compound_id, alternative_name AS name, alternative_name AS preferred_name, gene_aberration_occurences FROM drug_synonym INNER JOIN drug USING (drug_id) WHERE alternative_name LIKE ? "
            "ORDER BY name"
            , (name.strip(), ) * 2).fetchall()
    finally:
        cursor.close()


def query_disease(name):
    conn = get_db()
    cursor = conn.cursor()
  
    try:
       return cursor.execute(
            "SELECT disease_id AS id, full_name AS name, TCGA_disease, coalesce(TCGA_disease, full_name) AS preferred_name, gene_aberration_occurences "
            "FROM disease "
            "WHERE full_name LIKE ? OR TCGA_disease LIKE ? "
#            "UNION "
#            "SELECT disease_id AS id, alternative_name AS name, TCGA_disease, coalesce(TCGA_disease, alternative_name) AS preferred_name FROM disease_synonym INNER JOIN disease USING (disease_id) WHERE alternative_name LIKE ? "
            "ORDER BY TCGA_disease, name"
            , (name.strip(), ) * 2).fetchall()
    finally:
        cursor.close()


def query_gene_summary(name):
    conn = get_db()
    cursor = conn.cursor()
  
    try:
        return cursor.execute(
            "SELECT gene AS name, gene AS preferred_name, gene_aberration_occurences FROM gene_summary WHERE gene LIKE ? ORDER BY gene"
            , (name.strip(), )).fetchall()
    finally:
        cursor.close()


def query_database_summary():
    conn = get_db()
    cursor = conn.cursor()

    try:
        tables = cursor.execute(
            "SELECT t.article_id AS article_id, t.table_id AS id, t.name AS name, t.description AS description, rows "
            "FROM \"table\" t "
            "INNER JOIN (SELECT table_id, count(*) AS rows FROM gene_aberration GROUP BY table_id) AS stats USING (table_id) "
            "ORDER BY t.name"
        ).fetchall()

        tables_by_article = defaultdict(list)
        for table in tables:
            tables_by_article[table["article_id"]].append(table)

        articles = cursor.execute(
            "SELECT article_id, title FROM article ORDER BY title"
        ).fetchall()

        return [{"id": article["article_id"], "title": article["title"], "tables": tables_by_article[article["article_id"]]} for article in articles] 

    finally:
        cursor.close()


def extract_column_names(cur):
    return [i[0] for i in cur.description]


def result_to_html(args):
    return render_template("gene_aberration_search.html", **args)


def result_to_tsv(args):
    output = io.StringIO()

    aberrations = args["aberrations"]
    drugs = args["aberration_drugs"]
    diseases = args["aberration_diseases"]

    fieldnames = list(tuple(aberrations[0].keys()) + ("drugs", "diseases"))

    writer = csv.DictWriter(output, dialect='excel-tab', fieldnames=fieldnames)
    writer.writeheader()
    for row in aberrations:
        row = dict(row)
        row["drugs"] = ",".join((drug["name"] for drug in drugs[row["gene_aberration_id"]]))
        row["diseases"] = ",".join((disease["name"] for disease in diseases[row["gene_aberration_id"]]))
        writer.writerow(row)
    
    filename = "annitator-results.tsv"

    return (output.getvalue(), 200, {
        "Content-Type": "text/tab-separated-values",
        "Content-Disposition": 'attachment; filename="{}"'.format(filename)})


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = sqlite3.connect(db_file)
        g.sqlite_db.row_factory = sqlite3.Row
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()
