"use strict";

function init_terms() {

    function create_query_function(url_prefix) {
        return function (query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: url_prefix + encodeURIComponent(query),
                type: 'GET',
                error: function () {
                    callback();
                },
                success: function (res) {
                    callback(res.slice(0, 100));
                }
            });
        }
    }

    var options = {
        valueField: 'preferred_name',
        labelField: 'preferred_name',
        searchField: ['code', 'name'],
        // sortField: ['gene_aberration_occurences|desc', '$score'], // Doesn't work !?
        create: false,
        render: {
            option: function (item, escape) {
                console.log(item);
                return '<div>' +
                    (!item.name ? '<span>' + escape(item.preferred_name) + '</span>' :
                    '<span class="title">' +
                    (item.TCGA_disease ? ('<span class="code">' + escape(item.TCGA_disease) + '</span>') : '') +
                    '<span class="name">' + escape(item.name) + '</span>') +
                    '<span class="count">' + escape(item.gene_aberration_occurences) + '</span>' +
                    '</div>';
            }
        }
    }

    options.load = create_query_function("search/gene/");
    $("#gene-term").selectize(options);

    options.load = create_query_function("search/drug/");
    $("#drug-term").selectize(options);

    options.load = create_query_function("search/disease/");
    $("#disease-term").selectize(options);

}