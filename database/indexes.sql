--
-- Indexes and scripts that should be run after the data has been imported
-- 

CREATE INDEX gene_aberration_gene ON gene_aberration(gene);
CREATE INDEX gene_aberration_drug_drug_id ON gene_aberration_drug(drug_id);
CREATE INDEX gene_aberration_disease_disease_id ON gene_aberration_disease(disease_id);

-- The database is read-only. Statistics can be precalculated.
UPDATE drug SET gene_aberration_occurences = (SELECT count(*) FROM gene_aberration_drug WHERE drug_id = drug.drug_id);
UPDATE disease SET gene_aberration_occurences = (SELECT count(*) FROM gene_aberration_disease WHERE disease_id = disease.disease_id);

INSERT INTO gene_summary(gene, gene_aberration_occurences)
    SELECT gene, count(*) FROM gene_aberration GROUP BY gene ORDER BY gene;

ANALYZE;