#!/usr/bin/env python3

# Author: Kari Lavikka <kari.lavikka@helsinki.fi>

import yaml
import sqlite3
import sys
import csv
import re
import requests
import os
import argparse
import errno
import logging

from collections import defaultdict, Iterable
from os.path import normpath

from drug_finder import DrugFinder


SCRIPT_PATH = sys.path[0]

METADATA_FILENAME = "metadata.yaml"

# Paths are relative to the "database" root path, i.e. the dir of this script
PATHS = {k: normpath(os.path.join(SCRIPT_PATH, v)) for (k, v) in {
    "db": "annitator.db",
    "schema": "schema.sql",
    "indexes":  "indexes.sql",
    "diseases": os.path.join("auxiliary_data", "diseases.csv"),
    "disease_synonyms": os.path.join("auxiliary_data", "disease_synonyms.csv"),
    "gene_correction": os.path.join("auxiliary_data", "genename_correction_list.txt"),
    "unknown_pubchem_names": os.path.join("auxiliary_data", "unknown_pubchem_names.csv")
}.items()}

SUBSTITUTION_RE = re.compile(r"\$\{([^}]+)\}")
DRUG_STRUCTURAL_RE = re.compile(r"\d,\d")
DRUG_RE = re.compile(r"^([ ;+\-'\/\{\}\[\]\(\)a-zA-Z0-9]+?)(?: (\(.*\)))?$")

gene_correction_table = dict()
drug_mapper = lambda drug_id: 0 # Initialized later
disease_mapper = lambda disease_id: 0 # Initialized later

logger = logging.getLogger(__name__)

def main(args):
    args = parse_arguments()

    setup_logging(args.verbose)

    db_filename = args.o
    try:
        logger.info("Removing pre-existing database: %s", db_filename)
        os.remove(db_filename)
    except OSError:
        pass

    logger.info("Creating database: %s", db_filename)

    conn = open_db(db_filename)

    try:
        cursor = conn.cursor()

        create_schema(cursor)
        import_diseases(cursor)

        globals()["drug_mapper"] = create_drug_mapper(cursor)
        globals()["disease_mapper"] = create_disease_mapper(cursor)
        globals()["gene_correction_table"] = load_gene_corrections(PATHS["gene_correction"])

        import_directories(cursor, args.directories)

        create_indexes(cursor)

        log_statistics(cursor)

        logger.info("All done")

    finally:
        conn.close()


def parse_arguments():
    parser = argparse.ArgumentParser(description='Build an Annitator database.')

    parser.add_argument(
        'directories',
        metavar='dir',
        type=str,
        nargs='+',
        help='a directory to scan for ' + METADATA_FILENAME + ' files')

    parser.add_argument(
        '-o',
        metavar='database',
        type=str,
        default=PATHS['db'],
        help='where to store the database (default: {})'.format(PATHS['db'])
    )

    parser.add_argument(
        '--verbose',
        action='store_true',
        help='output debug logging')

    return parser.parse_args()


def setup_logging(verbose):
    level = logging.DEBUG if verbose else logging.INFO

    logging.getLogger('').setLevel(level)

    console = logging.StreamHandler()
    console.setLevel(level)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

def import_directories(cursor, directories):
    for dir in directories:
        if not os.path.exists(dir):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),
                                    dir)

        for root, dirs, files in os.walk(dir):
            if METADATA_FILENAME in files:
                import_directory(cursor, root)


def log_statistics(cursor):
    row_format = '{:30.30} {:5d}'
    
    msg = ''
    for row in cursor.execute("SELECT upper(full_name) AS name, count(*) AS count FROM disease INNER JOIN gene_aberration_disease USING (disease_id) WHERE TCGA_disease IS NULL GROUP BY upper(full_name) ORDER BY 2 DESC LIMIT 20"):
        msg += row_format.format(row[0], row[1]) + "\n"
    logger.info("Top unknown diseases:\n" + msg)

    msg = ''
    for row in cursor.execute("SELECT upper(name) AS name, count(*) AS count FROM drug INNER JOIN gene_aberration_drug USING (drug_id) WHERE pubchem_compound_id IS NULL AND pa_code IS NULL GROUP BY upper(name) ORDER BY 2 DESC LIMIT 20"):
        msg += row_format.format(row[0], row[1]) + "\n"
    logger.info("Top unknown drugs:\n" + msg)


def read_file(filename):
    with open(filename, "r") as f:
        return f.read()


def create_schema(cursor):
    cursor.executescript(read_file(PATHS["schema"]))


def import_diseases(cursor):
    sql = "INSERT INTO disease(TCGA_disease, full_name) VALUES(?, ?)"
    with open(PATHS["diseases"]) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            cursor.execute(sql, (row["tcga_code"].strip(), row["full_name"].strip()))

    sql = """INSERT INTO disease_synonym(alternative_name, disease_id)
        VALUES(?, (SELECT disease_id FROM disease WHERE TCGA_disease = ?))"""
    with open(PATHS["disease_synonyms"]) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            cursor.execute(sql, (row["alternative_name"].strip(), row["tcga_code"].strip()))


def create_indexes(cursor):
    cursor.executescript(read_file(PATHS["indexes"]))


def import_directory(cursor, directory):
    article = load_article(os.path.join(directory, METADATA_FILENAME))
    import_article(cursor, article, directory)


def import_article(cursor, article, directory):
    logger.info("* Importing article: %s", article["title"])

    # TODO: Harmonize keys in metadata.yaml and db schema
    sql = create_insert_statement("article", (
        "PMID",
        "title",
        "short_title",
        "very_short_title",
        "url",
        "source_type",
        "journal",
        "year",
        "doi",
        "abstract",
        "genome_version"))

    values = tuple((clean(article[k]) if k in article else None) for k in (
        "PMID",
        "title",
        "short_title",
        "very_short_title",
        "URL",
        "source_type",
        "journal",
        "year",
        "doi",
        "description",
        "genome_version",
    ))

    cursor.execute(sql, values)

    article_id = cursor.lastrowid

    if "tables" in article and isinstance(article["tables"], Iterable):
        for table in article["tables"]:
            import_table(cursor, table, article_id, directory)
    else:
        logger.warning(" * The article '%s' has no tables.", article["title"])


def import_table(cursor, table, article_id, directory):
    logger.info(" * Importing table: %s", table["name"])

    scalars = (
        "name",
        "description",
        "main_category"
    )

    sql = create_insert_statement("table", ("article_id", ) + scalars)

    values = (article_id, ) + tuple((clean(table[k]) if k in table else None) for k in scalars)

    cursor.execute(sql, values)
    table_id = cursor.lastrowid

    import_csv(cursor, table_id, table, directory)


def import_csv(cursor, table_id, table, directory):
    if not "column_mappings" in table or not isinstance(table["column_mappings"], dict) or len(table["column_mappings"]) <= 0:
        logger.warning("  * Column mappings of %s are missing. Skipping.", table["name"])
        return

    filename = os.path.join(directory, table["name"] + ".csv")
    column_mappings = table["column_mappings"]

    try:
        total_count = 0
        count = 0

        with open(filename, encoding='iso-8859-1') as csvfile:
            # Sniffing would be super cool, but it just does not seem to work:
            # dialect = csv.Sniffer().sniff(csvfile.readline())
            # csvfile.seek(0)
            dialect = "excel-tab" # TODO: Configurable in YAML?
            reader = csv.DictReader(csvfile, dialect=dialect)

            # Keys in metadata and database schema must be identical
            # TODO: Don't hard code the list of scalars here
            scalars = (
                "gene",
                "summary",
                "variant_protein",
                "variant_id",
                "variant_DNA",
                "cnv",
                "expression",
                "methylation",
                "clinical_significance",
                "evidence_level")

            required_scalars = ("gene", )

            sql = create_insert_statement("gene_aberration", ("table_id", "raw_id") + scalars)

            raw_importer = create_raw_importer(cursor, table["name"])

            def substitute_evidence(level):
                if level is None:
                    return None

                level = level.strip()
                if level == "":
                    return None

                try:
                    return table["evidence_mapping"][level]
                except KeyError:
                    logger.warning("  * Evidence level '%s' not found from mapping. Choices: %s",
                        level, ", ".join((str(es) for es in table["evidence_mapping"])))
                    return None


            def check_required_scalars(values):
                for required in required_scalars:
                    if values[scalars.index(required)] is None:
                        logger.debug("  * Missing value in required field: %s. Skipping row.", required)
                        return False
                return True


            # Callbacks for special column handling
            mutators = defaultdict(
                lambda: lambda x: x,
                gene=lambda x: gene_correction_table[x] if x in gene_correction_table else x,
                evidence_level=substitute_evidence if "evidence_mapping" in table else (lambda x: x))

            for row in reader:
                total_count += 1

                raw_id = raw_importer(row)

                values = tuple(
                    (mutators[s](
                        map_column(column_mappings[s], row) if s in column_mappings else None))
                    for s in scalars)

                if not check_required_scalars(values):
                    continue

                cursor.execute(sql, (table_id, raw_id) + values)

                aberration_id = cursor.lastrowid

                if "drug_name" in column_mappings:
                    import_aberration_drugs(cursor, aberration_id, map_column(column_mappings["drug_name"], row))

                if "disease" in column_mappings:
                    import_aberration_diseases(cursor, aberration_id, map_column(column_mappings["disease"], row))

                count += 1

        logger.info("  * Imported %d rows, skipped %d rows", count, total_count - count)

    except FileNotFoundError:
        logger.error("  * Can not open: %s! Skipping.", filename)

    except KeyError as e:
        logger.error("  * No such column: '%s'. Available: %s", e.args[0], ", ".join(row.keys()))


def map_column(template, row):
    # TODO: Return a mapper function

    if template is None:
        return None

    template = str(template)
    variables = extract_substitutions(template)
    # TODO: Create a tokenizer and concatenate instead of replacing
    for var in variables:
        template = template.replace("${" + var + "}", row[var].strip())

    template = template.strip()

    if template == "":
        template = None

    return template


def import_aberration_drugs(cursor, aberration_id, drug_string):
    if drug_string is not None and drug_string.strip():
        drugs = split_drugs(drug_string)

        sql = create_insert_statement("gene_aberration_drug",
            ("gene_aberration_id", "drug_id"))

        # Use set to get rid of possible duplicates
        for drug_id in set([drug_mapper(drug) for drug in drugs]):
            if drug_id is not None:
                cursor.execute(sql, (aberration_id, drug_id))


def import_aberration_diseases(cursor, aberration_id, disease_string):
    # TODO: This function is almost identical to import..drugs. Remove DRY

    if disease_string is not None and disease_string.strip():
        diseases = split_diseases(disease_string)

        sql = create_insert_statement("gene_aberration_disease",
            ("gene_aberration_id", "disease_id"))

        for disease_id in unique_ids(disease_mapper(disease) for disease in diseases):
            cursor.execute(sql, (aberration_id, disease_id))


def extract_substitutions(template):
    return [x.group(1) for x in SUBSTITUTION_RE.finditer(template)]


def create_insert_statement(table, columns):
    return "INSERT INTO \"{}\"({}) VALUES({})".format(
        table.replace("\"", "\\\""),
        ", ".join(("\"" + c + "\"" for c in columns)),
        ", ".join(("?" for s in columns)))


def clean(value):
    return value.strip() if isinstance(value, str) else value


def load_gene_corrections(filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile, dialect="excel-tab")
        return {row["Original_gene"]: row["Gene"] for row in reader}


def load_article(yaml_file):
    stream = open(yaml_file, 'r')
    return yaml.load(stream)


def split_drugs(drug_string):
    """Splits drug lists like: AZD8055,Binimetinib (MEK162),PD0325901,Everolimus,Selumetinib (AZD6244)"""

    # TODO: Multiple implementations of this function are probably needed.

    if DRUG_STRUCTURAL_RE.search(drug_string):
        # Assume structural formula like: 1,5-DIDEOXY-1,5-IMINO-D-MANNITOL
        return [drug_string]
    else:
        matches = [DRUG_RE.search(s.strip()) for s in drug_string.upper().split(sep=",")]
        parts = [m.group(1) for m in matches if m is not None]
        return parts


def create_drug_mapper(cursor):
    """Returns a function that maps drug names to drug ids"""

    # TODO: Populate drug_synonyms and allow search by any of the known names

    drug_finder = DrugFinder()
    imported_pubchem_drugs = dict() # pubchem compound id to surrogate key

    # Names that are known but can not be found from PubChem for some reason
    with open(PATHS["unknown_pubchem_names"]) as csvfile:
        unknown_pubchem_names = {row[1].upper(): row[0] for row in csv.reader(csvfile, delimiter='\t')}

    # Drugs that can not be found from PubChem nor are listed in the unknowns file.
    totally_unknown_names = requests.structures.CaseInsensitiveDict()

    drug_sql = create_insert_statement("drug", ("pubchem_compound_id", "name"))

    def find_and_add(name):
        if name.upper() in unknown_pubchem_names:
            name = unknown_pubchem_names[name.upper()]

        drug = drug_finder.find(name)
        if drug is not None:
            cid = drug["compound_id"]
            if cid in imported_pubchem_drugs:
                drug_id = imported_pubchem_drugs[cid]
            else:
                cursor.execute(drug_sql, (cid, drug["primary_name"]))
                drug_id = cursor.lastrowid
                imported_pubchem_drugs[cid] = drug_id
        else:
            if name in totally_unknown_names:
                drug_id = totally_unknown_names[name]
            else:
                logger.debug("  * Unknown drug: %s", name)
                cursor.execute(drug_sql, (None, name))
                drug_id = cursor.lastrowid
                totally_unknown_names[name] = drug_id

        return drug_id

    return find_and_add


def split_diseases(disease_string):
    # TODO: Multiple implementations of this function are probably needed.

    return [s.strip() for s in disease_string.upper().split(sep=",")]


def create_disease_mapper(cursor):
    """Returns a function that maps disease names to disease ids"""

    lookup = dict()

    for row in cursor.execute("SELECT disease_id, TCGA_disease, full_name FROM disease"):
        drug_id = row[0]
        lookup[row[1].upper()] = drug_id
        lookup[row[2].upper()] = drug_id

    for row in cursor.execute("SELECT disease_id, alternative_name FROM disease_synonym"):
        drug_id = row[0]
        lookup[row[1].upper()] = drug_id

    disease_sql = create_insert_statement("disease", ("full_name", ))

    def find_and_add(name):
        if name in lookup:
            return lookup[name]
        else:
            logger.debug("  * Unknown disease: %s", name)
            cursor.execute(disease_sql, (name, ))
            disease_id = cursor.lastrowid
            lookup[name] = disease_id
            return disease_id

    return find_and_add


def create_raw_importer(cursor, name):
    counter = 0
    sql = None

    def import_row(row):
        nonlocal counter, sql

        if counter == 0:
            columns = row.keys()
            creation_sql = 'CREATE TABLE "{}" (raw_id INTEGER PRIMARY KEY, {})'.format(
                name,
                ", ".join(['"' + column+ '" TEXT' for column in columns]))
            cursor.execute(creation_sql)

            sql = 'INSERT INTO "{}" VALUES({})'.format(name, ", ".join("?" * (len(row) + 1)))

        counter += 1

        cursor.execute(sql, (counter, ) + tuple(row.values()))
        return counter

    return import_row


def unique_ids(ids):
    return set((id for id in ids if id is not None))


def open_db(db_path):
    conn = sqlite3.connect(db_path)
    conn.execute("PRAGMA foreign_keys = ON;")
    return conn


if __name__ == "__main__":
    main(sys.argv)
