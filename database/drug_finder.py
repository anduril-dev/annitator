#!/usr/bin/env python3

# Resolves drug synonyms from PubChem
#
# Author: Kari Lavikka <kari.lavikka@helsinki.fi>
# 
# Tutorial to PubChem API: https://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST_Tutorial.html

import urllib
import sys
import atexit
import csv
import os
import logging

from time import time, strftime, sleep
from itertools import islice
import requests

logger = logging.getLogger(__name__)

class DrugFinder(object):
    """DrugFinder is a class that searches for compounds in PubChem and tries to
    solve the problem of synonyms.
    
    A compound can be searched by any of its names, and a compound id and a
    representative name are returned. The module caches the requests to speed
    up the lookup process. Cache files are designed so that they can be stored
    in a VCS."""

    URL_TEMPLATE = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{name}/description/JSON"
    MAX_REQUESTS_PER_SECOND = 5 # Let's be nice for the API

    CACHE_DIR = sys.path[0] + "/cache/"
    ID_TO_DRUG_CACHE_FILE = CACHE_DIR + "pubchem_drugs.tsv"
    NAME_TO_ID_CACHE_FILE = CACHE_DIR + "pubchem_synonyms.tsv"

    def __init__(self):
        self.name_to_id = requests.structures.CaseInsensitiveDict()
        self.drugs = {}
        self.last_request_date = 0
        self.startup_time_str = strftime("%Y-%m-%d")

        self._prefill_caches()
        self._open_cache_files_for_writing()

        atexit.register(self._cleanup)


    def _prefill_caches(self):
        if not os.path.exists(self.CACHE_DIR):
            os.makedirs(self.CACHE_DIR)

        try:
            with open(self.ID_TO_DRUG_CACHE_FILE, mode='r') as f:
                for row in csv.reader(f, delimiter='\t'):
                    self.drugs[int(row[0])] = { "primary_name": row[1] }
        except FileNotFoundError:
            pass

        try:
            with open(self.NAME_TO_ID_CACHE_FILE, mode='r') as f:
                for row in csv.reader(f, delimiter='\t'):
                    self.name_to_id[row[1]] = None if row[0] == "None" else int(row[0])
        except FileNotFoundError:
            pass


    def _open_cache_files_for_writing(self):
        self.drug_file = open(self.ID_TO_DRUG_CACHE_FILE, mode='a')
        self.synonym_file = open(self.NAME_TO_ID_CACHE_FILE, mode='a')


    def find(self, name):
        """Finds a drug from PubChem and returns a dict or None"""

        name = name.strip()

        try:
            return self._find_from_cache_by_name(name)

        except KeyError:
            cid = self._call_api(name)
            if cid is not None:
                return self._find_from_cache_by_cid(cid)
            else:
                return None


    def _find_from_cache_by_name(self, name):
        cid = self.name_to_id[name]
        if cid is not None:
            return self._find_from_cache_by_cid(cid)
        else:
            return None


    def _find_from_cache_by_cid(self, cid):
        drug = self.drugs[cid]

        return {
            "compound_id": cid,
            "primary_name": drug["primary_name"]
        }


    def _call_api(self, name):
        """Calls the API, adds records to cache and returns cid or None"""

        name = name.strip()

        self._throttle()

        logger.debug("Searching PubChem: '%s'", name)
        try:
            r = requests.get(self.URL_TEMPLATE.format(name=urllib.parse.quote(name, safe='')))

        except Exception:
            logger.warning("Connection error!")
            return None


        if r.status_code == 200:
            json = r.json()
            information = json["InformationList"]["Information"][0] # May contain several?

            cid = information["CID"]
            primary_name = information["Title"]

            logger.debug("Found, CID: %d, Title: %s", cid, primary_name)

            self._register_drug(cid, primary_name)
            self._register_synonym(cid, name)

            return cid
            
        elif r.status_code == 404:
            logger.info("Nothing found for %s", name)

            # Add missing synonyms to cache to prevent querying them repeatedly
            self._register_synonym(None, name)

            return None

        else:
            logger.warning("Error while fetching! Status code: %s", r.status_code)
            return None


    def _throttle(self):
        min_wait = 1 / self.MAX_REQUESTS_PER_SECOND

        wait = self.last_request_date + min_wait - time()
        if wait > 0:
            sleep(wait)

        self.last_request_date = time()


    def _register_drug(self, cid, primary_name):
        if cid not in self.drugs:
            self.drugs[cid] = { "primary_name": primary_name } # TODO: Date of update
            self.drug_file.write("{}\t{}\t{}\n".format(cid, primary_name, self.startup_time_str))


    def _register_synonym(self, cid, synonym):
        if synonym not in self.name_to_id:
            self.name_to_id[synonym] = cid
            self.synonym_file.write("{}\t{}\t{}\n".format(cid, synonym, self.startup_time_str))


    def _cleanup(self):
        self.drug_file.close()
        self.synonym_file.close()


if __name__ == "__main__":
    print("Testing....")

    df = DrugFinder()

    def test(name):
        print("* Searching: {}".format(name))
        print(df.find(name))

    test("Temazepam")
    test("temazePam")
    test("Planum")
    test("planum")
    test("Dasuen")
    test("xyzzy")
    test("xyzzy")
    test("Aspirin")
    test("Bevacizumab")
    test("Oxaliplatin")
    test("Eloxatin (TN)")
    test("Oxaliplatin (TN)")
    test("Everolimus")
    test("Capivasertib")


