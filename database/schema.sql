--
-- Schema for the database
-- Indexes are defined in the indexes.sql file
--

CREATE TABLE drug (
	drug_id INTEGER PRIMARY KEY,
	pubchem_compound_id INTEGER,
	pa_code TEXT COLLATE NOCASE, -- Not currently used
	name TEXT NOT NULL COLLATE NOCASE,	-- Primary name from PubChem
	gene_aberration_occurences INTEGER -- Precalculated row count
);
 
CREATE TABLE drug_synonym (
	alternative_name TEXT PRIMARY KEY COLLATE NOCASE,
	drug_id INTEGER NOT NULL,
	
	FOREIGN KEY (drug_id) REFERENCES drug(drug_id)
);
 
-- Diseases. This table contains disease names and abbreviations from TCGA.
-- In addition to those, all other (than cancer) diseases that appear in the
-- raw data will be added here.
CREATE TABLE disease (
	disease_id INTEGER PRIMARY KEY,
	TCGA_disease TEXT COLLATE NOCASE,
	full_name TEXT COLLATE NOCASE,
	gene_aberration_occurences INTEGER -- Precalculated row count
);
 
CREATE TABLE disease_synonym(
	alternative_name TEXT PRIMARY KEY COLLATE NOCASE,
	disease_id INTEGER NOT NULL,
	
	FOREIGN KEY (disease_id) REFERENCES disease(disease_id)
);
 
-- An article or an external database
CREATE TABLE article (
	article_id INTEGER PRIMARY KEY,
	title TEXT NOT NULL,
	short_title TEXT,
	very_short_title TEXT,
	source_type TEXT,
	journal TEXT,
	year INTEGER,
	abstract TEXT, -- description for article or db	
	doi TEXT,
	PMID TEXT,
	url TEXT,
	date_of_db_update TEXT,
	genome_version TEXT
);
 
-- Describes imported raw data that has been imported from
-- an external database or an article supplement
CREATE TABLE "table" (
	table_id INTEGER PRIMARY KEY,
	article_id INTEGER NOT NULL,
	name TEXT NOT NULL,
	description TEXT, -- description for outputs
	additional_info TEXT, -- what is this? Please describe.
	main_category TEXT, -- e.g. drug, prognosis (TEXT or ID from list?)
	 
	FOREIGN KEY (article_id) REFERENCES article(article_id)	
);
 
-- The names of the raw data tables are recorded in the “file” table
CREATE TABLE example_raw_data(
	raw_id	INTEGER PRIMARY KEY,
	diibadaa
);
 
-- Compilation/combined table, which contains data from multiple raw sources.
-- The data may be reformatted or processed in arbitrary ways.
-- table_leads to the original data source and raw_id is the specific
-- record in that source.
CREATE TABLE gene_aberration (
	gene_aberration_id INTEGER PRIMARY KEY,	-- a surrogate key
	table_id INTEGER,		-- where the original record is
	raw_id INTEGER,		-- id in the raw data table
	gene TEXT NOT NULL COLLATE NOCASE,		-- gene symbol
	summary TEXT,			-- human-readable description/summary
	clinical_significance TEXT,	-- e.g. efficacy, poor outcome, negative
	variant_protein TEXT,
	variant_id TEXT,
	haplotype_id TEXT,
	variant_DNA TEXT,
	cnv TEXT,
	expression TEXT,
	methylation TEXT,
	evidence_level INTEGER, -- Our own levels for evidence scoring: 0-5
	 
	FOREIGN KEY (table_id) REFERENCES "table"(table_id)
	-- raw_id can not have a foreign key. It references multiple tables.
);
 
-- Link drugs to gene_aberrations
CREATE TABLE gene_aberration_drug(
	gene_aberration_id INTEGER NOT NULL,
	drug_id INTEGER NOT NULL,
	raw_name TEXT, -- Name in original data
	
	PRIMARY KEY(gene_aberration_id, drug_id),
	FOREIGN KEY(gene_aberration_id) REFERENCES gene_aberration(gene_aberration_id),
	FOREIGN KEY(drug_id) REFERENCES drug(drug_id)
);
 
-- Link diseases to gene_aberrations
CREATE TABLE gene_aberration_disease(
	gene_aberration_id INTEGER NOT NULL,
	disease_id INTEGER NOT NULL,
	
	PRIMARY KEY(gene_aberration_id, disease_id),
	FOREIGN KEY(gene_aberration_id) REFERENCES gene_aberration(gene_aberration_id),
	FOREIGN KEY(disease_id) REFERENCES disease(disease_id)
);

-- CNVs and other data linked to genomic regions instead of genes
-- in new genome version hg38! convert first!
-- N.B. This is currently just an idea, not implemented
-- CREATE TABLE region_aberration (
-- 	region_id INTEGER PRIMARY KEY,
-- 	file_id INTEGER NOT NULL,
-- 	chr_band TEXT, 		-- chromosomal band (8q24) or arm (8q)
-- 	chr INTEGER NOT NULL,
-- 	position_start INTEGER NOT NULL,
-- 	position_end INTEGER NOT NULL,
-- 	summary TEXT,		-- human readable summary
--  
-- 	FOREIGN KEY (file_id) REFERENCES file(file_id)
-- );

-- List of all known genes
-- This mainly provides a quick prefix lookup for type-ahead search
CREATE TABLE gene_summary (
	gene TEXT PRIMARY KEY,
	gene_aberration_occurences INTEGER NOT NULL
);
