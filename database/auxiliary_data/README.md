# Raw auxiliary data

This folder contains auxiliary data such as diseases, commonly erroneous gene
names, etc.

## genename_correction_list.txt

The file contains mapping for gene names that are often messed up by excel,
for example.

## unknown_pubchem_names.csv

The file contains drug names that, for some reason, can not be found from
pubchem using that name. The first column is a name known to pubchem and the
second is that unknown name.

## diseases.csv

Diseases from TCGA

## disease_synonyms.csv

Some synonyms for diseases