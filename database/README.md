# The aggregated database

## Guidelines for importing data

We should preserve all raw data and scripts that are being used for importing
the data. This way it will be much easier to later figure out what has been
done, how and why. Sounds like a lab notebook?

## The data and the related metadata

The data is imported based on the definitions in a `metadata.yaml` file.
Metadata describes a source of the data (an article or a database), and one
or more tables of the actual data to be imported. The raw data files should
reside in the same directory as the `metadata.yaml` file.

An example of a `metadata.yaml` can be found at `/doc/example_metadata.yaml`.

## Auxiliary data

Auxiliary data is something that is not an actual part of the aggregated
data, but in some way supports it. For example, it may allow for better
search functionalities and a more usable user interface. It might also
facilitate the import procedure.

See `raw_auxiliary_data/README.md`

## Drug name harmonization

Drugs are commonly known by multiple names. During the import procedure, the
drugs are looked up from [PubChem][1] compounds and synonymous names are
replaced by the primary name that PubChem offers. If the name is not known to
PubChem, it will be stored as is.

Looking up the drugs takes some time on the first run, but subsequent imports
are speed up by cached data, which is stored in the `cache/` directory.

[1]: https://pubchem.ncbi.nlm.nih.gov/

## Building the database

Use `build.py`. The script expects to receive one or more directories as
command line arguments. The directories and their subdirectories are scanned
for `metadata.yaml` files and imported accordingly. Nested directories are
supported.

## Why SQLite?

The produced database is read-only. No sophisticated concurrency, locking or
multi-version concurrency control (MVCC) is required. An SQLite database is
easy to set up and manage and it does not require any running server
processes.

The database schema and SQL queries are mostly standard-compliant and using
more advanced DBMSes such as PostgreSQL or MySQL should be trivial.