#!/usr/bin/env python3

# Reads variants from stdin and prints the variant and match count to stdout

import requests
import sys
import re

SERVER="http://localhost:5000" # TODO: Command line arg

for line in sys.stdin:
    line = line.strip()
    match = re.search('[a-zA-Z0-9]+:p\.\w\d+', line)
    if match:
        line = match.group(0)
        r = requests.get(SERVER + "/", params={"variant": line, "format": "count"})
        print(line + "\t" + r.text)
    else:
        print(line + "\t" + "<- invalid")


